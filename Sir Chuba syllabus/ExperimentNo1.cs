/*
* Program to illustrate boxing and unboxing
*/

using System;

namespace ExperimentNo1
{
	class MainClass
	{
		public static void Main (string [] args)
		{
			Console.WriteLine ("Program to illustrate boxing and unboxing");
			int m = 100;
			object om = (object)m;
			Console.WriteLine ("Value of value type m is {0}", m);
			Console.WriteLine ("Value of object type om is {0}", om);
			m = 200;
			Console.WriteLine ("Value of value type m is {0}", m);
			Console.WriteLine ("Value of object type om is {0}", om);
			int p = (int)om;
			Console.WriteLine ("Value of value type m is {0}", m);
			Console.WriteLine ("Value of value type p is {0}", p);
			Console.WriteLine ("Value of object type om is {0}", om);
		}
	}
}