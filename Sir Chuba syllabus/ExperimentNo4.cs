/*
 * ArrayList demonstration:
 * deleting and interchanging
 */

using System;
using System.Collections;

namespace ExperimentNo4
{
	class MainClass
	{
		const int SIZE = 3;
		static string[] name = new string[SIZE];
		static string[] course = new string[SIZE];
		static string temp;
		static int[] roll = new int[SIZE];

		public static void Main (string[] args)
		{
			ArrayList arr = new ArrayList();
			PrintList(arr);
			Get(arr);
			PrintList(arr);

			Console.Write("To remove 2nd student detail press Y/y or N/n to Interchange 1st and 3rd student detail: ");
			char option = Convert.ToChar (Console.ReadLine ());
			if (option == 'Y' || option == 'y') {
				Delete(arr);
			} else {
				Interchange(arr);
			}

			PrintList(arr);
		}

		// print the list
		static void PrintList(ArrayList arr)
		{
			Console.WriteLine("Current capacity of ArrayList: " + arr.Capacity);
			Console.WriteLine("Current elements are: " + arr.Count);
			for (int i = 0; i < arr.Count; i++) {
				Console.WriteLine(arr[i]);
			}
			Console.WriteLine();
		}

		// Get the elements
		static void Get(ArrayList arr)
		{
			for (int i = 0; i < SIZE; i++) {
				Console.WriteLine("Enter detail of student " + (i+1));
				Console.Write("Enter name: ");
				name[i] = Console.ReadLine();
				arr.Add(name[i]);
				Console.Write("Enter roll: ");
				temp = Console.ReadLine();
				roll[i] = int.Parse(temp);
				arr.Add(roll[i]);
				Console.Write("Enter course: ");
				course[i] = Console.ReadLine();
				arr.Add(course[i]);
			}
		}

		// Delete 2nd student detail
		static void Delete(ArrayList arr)
		{
			Console.WriteLine("Deleting elements of 2nd student...");
			arr.Remove(name[1]);
			arr.Remove(roll[1]);
			arr.Remove(course[1]);
		}

		// Interchange 1st and 3rd student
		static void Interchange(ArrayList arr)
		{
			Console.WriteLine("Interchanging elements...");
			arr[0] = name[2];
			arr[1] = roll[2];
			arr[2] = course[2];
			arr[6] = name[0];
			arr[7] = roll[0];
			arr[8] = course[0];
		}
	}
}
