/*
 * Getter and Setter
 */

using System;

namespace ExperimentNo8
{
	class Student
	{
		private string code = "N.A";
		private string name = "not known";
		private int age = 0;

		// property of type string
		public string Code {
			get {
				return code;
			}
			set {
				code = value;
			}
		}

		public string Name {
			get {
				return name;
			}
			set {
				name = value;
			}
		}

		public int Age {
			get {
				return age;
			}
			set {
				age = value;
			}
		}

		public override string ToString ()
		{
			return "Code = " + this.Code + ", Name = " + this.Name + ", Age = " + this.Age;
		}
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
			Student s = new Student();

			// set values
			s.Code = "001";
			s.Name = "Zara";
			s.Age = 9;
			Console.WriteLine("Student info: {0}", s);

			// update values
			s.Code = s.Code + 3;
			s.Age = s.Age + 3;
			Console.WriteLine("Student info: {0}", s);
		}
	}
}
