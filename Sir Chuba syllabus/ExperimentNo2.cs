﻿/*
 * Write a program to demonstrate arithmetic operation
 */

using System;

namespace ExperimentNo2
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int sum = 0, difference = 0, product = 0, modulo = 0;
			float quotient = 0;

			Console.WriteLine("Enter any two numbers:");
			string x = Console.ReadLine();
			string y = Console.ReadLine();
			int num1 = int.Parse(x);
			int num2 = int.Parse(y);

			sum = num1 + num2;
			difference = num1 - num2;
			product = num1 * num2;
			modulo = 3 % num2;
			quotient = num1 / num2;

			Console.WriteLine("num1 = {0}, num2 = {1}", num1, num2);
			Console.WriteLine();
			Console.WriteLine("sum of {0} and {1} is {2}", num1, num2, sum);
			Console.WriteLine("difference of {0} and {1} is {2}", num1, num2, difference);
			Console.WriteLine("product of {0} and {1} is {2}", num1, num2, product);
			Console.WriteLine("quotient of {0} and {1} is {2}", num1, num2, quotient);
			Console.WriteLine();
			Console.WriteLine("remainder when 3 is divided by {0} is {1}", num2, modulo);

			num1++;
			num2--;

			Console.WriteLine("num1 = {0}, num2 = {1}", num1, num2);
			Console.WriteLine("num1 = {0}, num2 = {1}", num1, num2);
		}
	}
}
