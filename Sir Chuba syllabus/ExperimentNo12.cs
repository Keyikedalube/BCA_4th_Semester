/*
 * try-catch program
 */

using System;

namespace ExperimentNo12
{
	class NoNagaException : ApplicationException
	{
		public NoNagaException () : base ("We do not allow naga names  here!")
		{
			this.HelpLink = "https://www.naganames.com";
		}
	}
	class MainClass
	{
		public static string GetName ()
		{
			Console.Write ("Enter your name: ");
			string s = Console.ReadLine ();
			if (s.Equals ("naga"))
				throw new NoNagaException ();
			return s;
		}

		public static void Main (string[] args)
		{
			string Name;
			try
			{
				Name = GetName();
				Console.WriteLine ("hello {0}", Name);
			}
			catch (NoNagaException e)
			{
				Console.WriteLine (e.Message);
				Console.WriteLine ("Please visit {0} for further  details", e.HelpLink);

				Console.WriteLine ("Source:{0}", e.Source);
				Console.WriteLine ("Stack:{0}", e.StackTrace);
				// e.Data.Add("TimeStamp:(string.Format("Exception occured at {0}",e.DateTime.now)));
			}
			finally
			{
				Console.WriteLine ("Have a Cold Day!!!");
			}
		}
	}
}
