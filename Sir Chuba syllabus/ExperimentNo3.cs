/*
 * Write a program to calculate the grade of a student using
 * else-if ladder
 */

using System;

namespace ExperimentNo3
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			int maths = 0, science = 0, english = 0, social_science = 0, hindi = 0;

			Console.WriteLine("Calculation of grade");
			Console.WriteLine("Enter the marks of the subjects which you have scored in your test");
			Console.Write("Maths: ");
			maths = Convert.ToInt32(Console.ReadLine());
			Console.Write("Science: ");
			science = Convert.ToInt32(Console.ReadLine());
			Console.Write("English: ");
			english = Convert.ToInt32(Console.ReadLine());
			Console.Write("Social science: ");
			social_science = Convert.ToInt32(Console.ReadLine());
			Console.Write("Hindi: ");
			hindi = Convert.ToInt32(Console.ReadLine());

			int total = maths + science + social_science + english + hindi;
			float percentage = total / 5;

			// ladder else-if
			if (percentage > 84)
				Console.WriteLine("You have scored {0}% and your grade is honors", percentage);
			else if (percentage > 79)
				Console.WriteLine("You have scored {0}% and your grade is A", percentage);
			else if (percentage > 59)
				Console.WriteLine("You have scored {0}% and your grade is B", percentage);
			else if (percentage > 49)
				Console.WriteLine("You have scored {0}% and your grade is C", percentage);
			else if (percentage > 39)
				Console.WriteLine("You have scored {0}% and your grade is D", percentage);
			else
				Console.WriteLine("You have scored {0}% and you have failed in your test!", percentage);
		}
	}
}
