/*
 * Program demonstrating enum
 */

using System;

namespace ExperimentNo11
{
	struct DateOfBirth
	{
		public int Date;
		public int Year;
	}

	enum Months : byte
	{
		January = 01,	// making the month of january as 1 
		February,
		March,
		April,
		May,
		June,
		July,
		August,
		September,
		October,
		November,
		December
	}

	class MainClass
	{
		public static void Main (string[] args)
		{
			DateOfBirth dob;
			dob.Date = 15;
			dob.Year = 1996;
			int j = (int) Months.December; // explicitly defined so as to display the integer and not the string or char
			Months m1 = Months.December;
			Console.Write("Date of Birth is: {0}", dob.Date);
			Console.Write("/{0}({1})", m1, j);
			Console.WriteLine("/{0}", dob.Year);
			Console.WriteLine();
			Console.Write("The Size of Enum <Months> is: ");
			Console.WriteLine(sizeof(Months));
		}
	}
}
