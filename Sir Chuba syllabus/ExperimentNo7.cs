using System;

namespace ExperimentNo7
{
	class MainClass
	{
		public static float Mul (float x, float y)
		{
			return x * y;
		}

		public static float Div (float x, float y)
		{
			return x / y;
		}

		public static void Main (string[] args)
		{
			float a = MainClass.Mul(10f, 20f);
			float b = MainClass.Div(20.1f, 2f);

			Console.WriteLine("Mul result is: {0}", a);
			Console.WriteLine("Div result is: {0}", b);
		}
	}
}
