/*
 * Program demonstrating jagged array
 */

using System;

namespace ExperimentNo5
{
	class MainClass
	{
		int[][] JaggedArray;
		int Row, Column;

		// get row and column size
		public void GetRowAndColumnSize ()
		{
			Console.Write ("Enter no. of rows: ");
			JaggedArray = new int[Convert.ToInt32 (Console.ReadLine ())][];
			Console.WriteLine ();
			for (int index = 0; index < JaggedArray.Length; index++) {
				Console.Write ("Enter the no. of column(s) for row {0}: ", index + 1);
				JaggedArray [index] = new int[Convert.ToInt32 (Console.ReadLine ())];
			}
			Console.WriteLine ();
		}

		// get jagged array values
		public void GetRowAndColumnValues ()
		{
			for (Row = 0; Row < JaggedArray.Length; Row++) {
				for (Column = 0; Column < JaggedArray [Row].Length; Column++) {
					Console.Write ("Enter values for row {0} column {1}: ", Row + 1, Column + 1);
					JaggedArray [Row] [Column] = Convert.ToInt32 (Console.ReadLine ());
				}
			}
		}

		// print jagged array
		public void PrintJaggedArray ()
		{
			Console.WriteLine ();
			Console.WriteLine ("The jagged array is:");
			for (Row = 0; Row < JaggedArray.Length; Row++) {
				for (Column = 0; Column < JaggedArray [Row].Length; Column++) {
					Console.Write (JaggedArray [Row] [Column]);
				}
				Console.WriteLine ();
			}
		}

		public static void Main (string[] args)
		{
			MainClass JaggedArray = new MainClass ();
			JaggedArray.GetRowAndColumnSize ();
			JaggedArray.GetRowAndColumnValues ();
			JaggedArray.PrintJaggedArray ();
		}
	}
}
