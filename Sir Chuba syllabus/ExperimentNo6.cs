/*
 * Program to demonstrate overloaded constructor
 */

using System;

namespace ExperimentNo6
{
	class Room
	{
		public double Length;
		public double Breadth;
		
		public Room (double x, double y)
		{
			Length = x;
			Breadth = y;
		}
		
		public Room (double a)
		{
			Length = Breadth = a;
		}
		
		public double Area ()
		{
			double x = Length * Breadth;
			return x;
		}
	}
	
	class MainClass
	{
		public static void Main (string[] args)
		{
			Room rectangle = new Room (10f, 20f);
			double a = rectangle.Area ();

			Room square = new Room(30f);
			double b = square.Area ();

			Console.WriteLine ("Area of rectangle: {0}", a);
			Console.WriteLine ("Area of square: {0}", b);
		}
	}
}
